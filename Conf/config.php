<?php
$baseCFG = require THINK_PATH.'../config.php';
$addCFG =  array(
	'DEFAULT_THEME' => 'default',
	'TMPL_ACTION_ERROR'     => THINK_PATH.'../Tpl/Jump.tpl.php',
	'TMPL_ACTION_SUCCESS'   => THINK_PATH.'../Tpl/Jump.tpl.php',
	'TMPL_EXCEPTION_FILE'   => THINK_PATH.'../Tpl/Exception.tpl.php',
	'TMPL_TEMPLATE_SUFFIX'  => '.php',
	'APP_AUTOLOAD_PATH'     => 'ORG.Util.,ORG.Crypt.',
);
return array_merge($baseCFG,$addCFG);
?>