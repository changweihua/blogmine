<?php
return array(
	'DB_TYPE' => 'mysql',
	'DB_HOST' => 'localhost',
	'DB_NAME' => 'blogmine',
	'DB_USER' => 'root',
	'DB_PWD' => 'microsoft',
	'DB_PORT' => '3306',
	'DB_PREFIX' => 'blogmine_',
	
	'URL_MODEL' => 2,
	
	'TMPL_PARSE_STRING' => array(
		'__ROOT__' => BLOGMINE_URI,
		'__PUBLIC__' => BLOGMINE_URI.'/Public',
	),
);