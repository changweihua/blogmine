<?php
class MainAction extends Action {
	public function __construct(){
		parent::__construct();
		$this->SystemInit();
	}
	public function SystemInit(){
		$this->LoadSetting();
	}
	public function LoadSetting(){
		C('BLOGMINE_SETTING',A('DB')->Setting_Get());
	}
	
	/**
	 * 获取设定项值
	 * @param string $key
	 * @return string
	 */
	public function getSetting($key){
		return C('BLOGMINE_SETTING.'.$key);
	}
}