<?php
class DBAction extends Action {
	public function __construct(){
		parent::__construct();
	}
	/* === [Prefix]setting start === */
	/**
	 * 获取所有设定
	 * @return array
	 */
	public function Setting_Get(){
		$dbData = M('Setting')->select();
		$settings = array();
		foreach($dbData as $val) $settings[$val['key']] = $val['value'];
		return $settings;
	}
	/* === [Prefix]setting end === */
}